# ZIA App Builder

ZIA App Builder adalah aplikasi builder yang tujuannya mempermudah software developer
dalam membuat aplikasi.

## Spesifikasi

Core:

- PHP 8.1
- Codeigniter 4x
- MySQL
- Javascript

Secondary:

- JQuery, karena menggunakan datatable.
- Datatable.

## Roadmap

- [] Menambah fitur di Codeigniter sehingga developer dapat berfokus di code PHP/Codeigniter saja. Tanpa perlu mengkoding HTML dan Javascript.
- [] Generate Reporting.
- [] Membuat sistem tanpa menyentuh kode sumber.
- [] Membuat sistem dengan hanya mengupload desain sistem.
- [] Database agnostic. Dukungan selain database MySQL.
- [] Memutakhirkan UI/UX. Mereplace JQuery dan Datatable.
- [] Memisahkan antara UI dan Backend.
- [] Integrasi dengan sistem ERP seperti Odoo.
